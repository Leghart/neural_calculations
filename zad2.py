import os
from contextlib import redirect_stdout

import numpy as np
from tensorflow.keras import layers, models

from argparsers.args_zad2 import ps
from utils import plot_MSE, plot_prediction_errors, plot_system_model

os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"
sys = vars(ps)


def system(u, noise=False, sigma=1.5):
    y = np.zeros(u.size)

    for k in range(2, len(u)):
        y[k] = 0.6 * y[k - 1] + 0.1 * y[k - 2] + (u[k - 1]) ** 2 + 0.5 * u[k - 2]
    if noise:
        y += np.random.normal(0.0, sigma, u.size)

    return y


def prepare_dataset(u, y, system):
    assert u.size == y.size
    X = [y[1:-2], y[0:-3], u[1:-2], u[0:-3]] if system == "NNARX" else [u[1:-2], u[0:-3]]

    X = np.array(X)
    X = X.T

    T = y[2:-1]
    T = np.array(T)

    return X, T


def build_network(X, T, system, input_neurons, hidden_neurons):
    model = models.Sequential()

    input_shape = (4,) if system == "NNARX" else (2,)

    model.add(layers.Dense(input_neurons, input_shape=input_shape, activation="tanh"))
    model.add(layers.Dense(hidden_neurons, activation="tanh"))
    model.add(layers.Dense(1, activation="linear"))

    model.compile(loss="mean_squared_error", optimizer="sgd", metrics=["mean_squared_error"])

    history = model.fit(X, T, epochs=50, batch_size=100, verbose=1, validation_split=0.2)
    with open(f"SUMMARIES/{system}__inneurons_{input_neurons}__hidneurons_{hidden_neurons}.txt", "w") as f:
        with redirect_stdout(f):
            model.summary()

    return model, history, X, T


if sys["m"] == "nnfir":

    N = 10000
    u = np.random.uniform(low=0.0, high=2.0, size=N)
    y = system(u)

    X, T = prepare_dataset(u[10:], y[10:], "NNFIR")

    model, history, X, T = build_network(X, T, "NNFIR", input_neurons=sys["i"], hidden_neurons=sys["nn"])

    Y_hat = model.predict(X)
    T.shape
    X.shape
    (Y_hat[:, 0]).shape
    errors = T - Y_hat[:, 0]

    plot_MSE(history=history, system_type="NNFIR", directory="ZAD2_badania", sys=sys)
    plot_system_model(Y_hat, T, "NNFIR", "ZAD2_badania", sys=sys)
    plot_prediction_errors(errors, "NNFIR", "ZAD2_badania", sys=sys)
    # save_logs("NNFIR", Y_hat, errors, T, X)

if sys["m"] == "nnarx":

    N = 10000
    u = np.random.uniform(low=0.0, high=2.0, size=N)
    y = system(u)

    X, T = prepare_dataset(u[10:], y[10:], "NNARX")

    model, history, X, T = build_network(X, T, "NNARX", input_neurons=sys["i"], hidden_neurons=sys["nn"])

    Y_hat = model.predict(X)
    T.shape
    X.shape
    (Y_hat[:, 0]).shape
    errors = T - Y_hat[:, 0]

    plot_MSE(history=history, system_type="NNARX", directory="ZAD2_badania", sys=sys)
    plot_system_model(Y_hat, T, "NNARX", "ZAD2_badania", sys=sys)
    plot_prediction_errors(errors, "NNARX", "ZAD2_badania", sys=sys)
    # save_logs("NNARX", Y_hat, errors, T, X)
