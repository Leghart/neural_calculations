import os

import matplotlib.pyplot as plt
from sklearn.metrics import ConfusionMatrixDisplay, confusion_matrix


def plot(history, filename, path):
    # pattern: dataset_optimizer_layers_neurons_[dropout]_[augment]
    print(history)
    fig, axs = plt.subplots(2, figsize=(16, 9))
    axs[0].plot(history.history["accuracy"], label="accuracy")
    axs[0].plot(history.history["val_accuracy"], label="val_accuracy")
    axs[0].set_title("Accuracy of learning")
    axs[0].set_xlabel("Epoch")
    axs[0].set_ylabel("Accuracy")
    axs[0].legend(loc="lower right")
    axs[0].grid(which="major")

    axs[1].plot(history.history["loss"], label="loss")
    axs[1].plot(history.history["val_loss"], label="val_loss")
    axs[1].set_title("Loss")
    axs[1].set_xlabel("Epoch")
    axs[1].set_ylabel("Loss")
    axs[1].legend(loc="lower right")
    axs[1].grid(which="major")

    # axs[1].plot()
    fig.savefig(path + "/" + filename + ".png")


def plot_confusion_matrix(dataset, filename, path):
    predictions = dataset.model.predict(dataset.test_images)
    y_pred = predictions > 0.5

    matrix = confusion_matrix(dataset.test_labels, y_pred.argmax(axis=1))
    disp = ConfusionMatrixDisplay(confusion_matrix=matrix)
    disp.plot(cmap=plt.cm.BuPu)

    plt.savefig(f"{path}/{filename}_matrix.png")


def plot_MSE(history, system_type, directory, sys, save=True):
    plt.plot(history.history["loss"], label="train_loss")
    plt.plot(history.history["val_loss"], label="val_loss")
    plt.xlabel("Epochs")
    plt.ylabel("MSE")
    plt.legend()
    plt.grid(True)
    if save:
        filename = f"{system_type}_MSE_inneurons_{sys['i']}__hidneurons_{sys['nn']}"
        name = os.path.join(directory, filename)
        plt.savefig(name + ".png")
        plt.close()
    else:
        plt.show()


def plot_system_model(Y_hat, T, system_type, directory, sys, save=True):
    plt.plot(Y_hat[200:300], "r", label="Model output")
    plt.plot(T[200:300], label="System output")
    plt.legend()
    plt.grid(True)
    if save:
        filename = f"{system_type}_system_model_inneurons_{sys['i']}__hidneurons_{sys['nn']}"
        name = os.path.join(directory, filename)
        plt.savefig(name + ".png")
        plt.close()
    else:
        plt.show()


def plot_prediction_errors(errors, system_type, directory, sys, save=True):
    plt.hist(errors)
    plt.grid(True)
    if save:
        filename = f"{system_type}_prediction_errors_inneurons_{sys['i']}__hidneurons_{sys['nn']}"
        name = os.path.join(directory, filename)
        plt.savefig(name + ".png")
        plt.close()
    else:
        plt.show()


def save_logs(typ_sieci, Y_hat, errors, T, X):
    import numpy as np

    Y_hat[:, 0]
    MSE = np.mean(errors**2)

    n = len(T)
    n2 = n // 2  # w pythonie operator // oznacza dzielenie bez reszty

    T_train = T[:n2]
    X_train = X[:n2, :]

    T_test = T[n2:]
    X_test = X[n2:, :]

    with open(f"{typ_sieci}_logs.txt", "w+") as f:
        f.write(f"MSE: {MSE}\n")
        f.write(f"X_train: {X_train.shape}\n")
        f.write(f"T_train: {T_train.shape}\n")
        f.write(f"X_test: {X_test.shape}\n")
        f.write(f"T_test: {T_test.shape}\n")
