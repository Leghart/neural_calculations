import os

"""Stockowo sieć posiada 2 dodatkowe warstwy, 10 neuronów, 10 epoch oraz korzysta z optimizera ADAM"""
basic_layers_number = 1
basic_neuron_number = 32
basic_epoch_number = 10
basic_optimizer = "adam"
basic_dataset = "cifar10"


# """Wpływ rozmiaru sieci - liczba warstw oraz liczba neuronów"""

# neuron_number = [4, 32, 64]
# print("**********************************************")
# print("\nTESTING NUMBER OF NEURONS\n")
# print("**********************************************")

# for i in neuron_number:
#     print(f"\n\n{i} NEURONS\n\n")

#     os.system(
#         f"python .\zad1.py -ds {basic_dataset} -ln {basic_layers_number} -nn {i} -o {basic_optimizer} -e {basic_epoch_number}"
#     )

# layers = [3]
# # print("**********************************************")
# # print("\nTESTING NUMBER OF LAYERS\n")
# # print("**********************************************")

# for i in layers:
#     print(f"\n\n{i} Layers\n\n")

#     os.system(
#         f"python .\zad1.py -ds {basic_dataset} -ln {i} -nn {basic_neuron_number} -o {basic_optimizer} -e {basic_epoch_number}"
#     )


# """Wpływ regulacji przez porzucenie i różne wartości dropout"""
# dropout_values = [0.01, 0.5, 0.9]
# print("**********************************************")
# print("\nTESTING DROPOUT RATE\n")
# print("**********************************************")

# for i in dropout_values:
#     print(f"\n\n{i} DROPOUT RATE\n\n")

#     os.system(
#         f"python .\zad1.py -ds {basic_dataset} -ln {basic_layers_number} -nn {basic_neuron_number} -o {basic_optimizer} -e {basic_epoch_number} -dr {i}"
#     )


# """Wpływ augmentacji danych"""

print("\n\nData Augmentation\n\n")

os.system(
    f"python .\zad1.py -ds cifar10 -ln {basic_layers_number} -nn {basic_neuron_number} -o {basic_optimizer} -e {basic_epoch_number}"
)

# os.system(
#     f"python .\zad1.py -ds cifar10 -ln {basic_layers_number} -nn {basic_neuron_number} -o {basic_optimizer} -e {basic_epoch_number}"
# )

# """Porównanie optimizerów"""

# optimizer_list = ["adam", "sgd", "sgd_momentum"]

# print("**********************************************")
# print("\nTESTING OPTIMIZERS\n")
# print("**********************************************")

# for i in optimizer_list:
#     print(f"\n\n{i} OPTIMIZER\n\n")

#     os.system(
#         f"python .\zad1.py -ds {basic_dataset} -ln {basic_layers_number} -nn {basic_neuron_number} -o {i} -e {basic_epoch_number}"
#     )


# ####################################################
# ###################### ZAD 2 #######################
# ####################################################
# """ Default input data"""
# model = "nnarx"
# neurons = 10
# inputs = 20

# ############ Change inputs ###############
# inputs = (10, 30, 60)
# for i in inputs:
#     print(f"\n\n{i} INPUTS\n\n")
#     os.system(f"python .\zad2.py -m {model} -nn {neurons} -i {i}")

# ############ Change neurons ###############
# neurons = (10, 30, 60)
# inputs = 20
# for n in neurons:
#     print(f"\n\n{i} Neurons\n\n")
#     os.system(f"python .\zad2.py -m {model} -nn {n} -i {inputs}")


# model = "nnfir"
# neurons = 10
# ############ Change inputs ###############
# inputs = (10, 30, 60)
# for i in inputs:
#     print(f"\n\n{i} INPUTS\n\n")
#     os.system(f"python .\zad2.py -m {model} -nn {neurons} -i {i}")

# ############ Change neurons ###############
# neurons = (10, 30, 60)
# inputs = 20
# for n in neurons:
#     print(f"\n\n{i} Neurons\n\n")
#     os.system(f"python .\zad2.py -m {model} -nn {n} -i {inputs}")
