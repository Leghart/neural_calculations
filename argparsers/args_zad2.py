import argparse

parser = argparse.ArgumentParser(description="")
parser.add_argument("-m", action="store", help="kind of model")
parser.add_argument("-nn", action="store", help="neuron number", type=int)
parser.add_argument("-i", action="store", help="number of inputs", type=int)

ps = parser.parse_args()
