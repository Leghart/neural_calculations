import argparse

parser = argparse.ArgumentParser(description="")
parser.add_argument("-ds", action="store", help="dataset")
parser.add_argument("-ln", action="store", help="layers number", type=int)
parser.add_argument("-nn", action="store", help="neuron number", type=int)
parser.add_argument("-o", action="store", help="optimizer")
parser.add_argument("-e", action="store", help="epoches", type=int)
parser.add_argument("-dr", action="store", default=0, help="dropout rate", type=float)
parser.add_argument("-aug", action="store", default=False, help="data augmentation", type=bool)

ps = parser.parse_args()
