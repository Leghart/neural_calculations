import os
from contextlib import redirect_stdout

import tensorflow as tf

from argparsers.args_zad1 import ps
from models import CIFAR10, CIFAR100, MNIST
from utils import plot, plot_confusion_matrix

os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"
sys = vars(ps)


def select_optimizer(sys):
    print("OPTIMIZER str: ", sys["o"])
    if sys["o"] == "adam":
        return tf.keras.optimizers.Adam(learning_rate=0.001)

    if sys["o"] == "sgd":
        return tf.keras.optimizers.SGD(momentum=0.0)

    if sys["o"] == "sgd_momentum":
        return tf.keras.optimizers.SGD(momentum=0.9)


if sys["ds"] == "mnist":
    dataset = MNIST(layers_num=sys["ln"], neurons=sys["nn"], dropout=sys["dr"])

    dataset.model.compile(
        optimizer=select_optimizer(sys),
        loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
        metrics=["accuracy"],
    )
    if not sys["aug"]:
        print("Normal data")
        history = dataset.model.fit(
            dataset.train_images,
            dataset.train_labels,
            epochs=sys["e"],
            validation_data=(dataset.test_images, dataset.test_labels),
        )
    else:
        print("Data augmentation")

    plot(
        history,
        f"{sys['ds']}__opt_{sys['o']}__layernum_{sys['ln']}__neurons_{sys['nn']}__droprate_{sys['dr']}__aug{sys['aug']}",
        "ZAD1_badania",
    )
    plot_confusion_matrix(
        dataset,
        f"{sys['ds']}__opt_{sys['o']}__layernum_{sys['ln']}__neurons_{sys['nn']}__droprate_{sys['dr']}__aug{sys['aug']}",
        "ZAD1_badania",
    )

    with open(
        f"SUMMARIES/MNIST__neurons_{sys['nn']}__dropout_{sys['dr']}__optimizer_{sys['o']}__aug_{sys['aug']}.txt",
        "w",
    ) as f:
        with redirect_stdout(f):
            dataset.model.summary()


elif sys["ds"] == "cifar10":
    dataset = CIFAR10(layers_num=sys["ln"], neurons=sys["nn"], dropout=sys["dr"])
    dataset.model.compile(
        optimizer=select_optimizer(sys),
        loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
        metrics=["accuracy"],
    )
    if not sys["aug"]:
        history = dataset.model.fit(
            dataset.train_images,
            dataset.train_labels,
            epochs=sys["e"],
            validation_data=(dataset.test_images, dataset.test_labels),
        )
    else:
        print("Data augmentation")
        history = dataset.model.fit(
            dataset.it_train,
            epochs=sys["e"],
            validation_data=(dataset.test_images, dataset.test_labels),
        )
    print("SIEMA: ", sys["dr"])
    plot(
        history,
        f"{sys['ds']}__opt_{sys['o']}__neurons_{sys['nn']}__droprate_{sys['dr']}__aug{sys['aug']}",
        "ZAD1_badania",
    )
    plot_confusion_matrix(
        dataset,
        f"{sys['ds']}__opt_{sys['o']}__neurons_{sys['nn']}__droprate_{sys['dr']}__aug{sys['aug']}",
        "ZAD1_badania",
    )

    with open(
        f"SUMMARIES/CIFAR10__neurons_{sys['nn']}__dropout_{sys['dr']}__optimizer_{sys['o']}__aug_{sys['aug']}.txt",
        "w",
    ) as f:
        with redirect_stdout(f):
            dataset.model.summary()


elif sys["ds"] == "cifar100":
    dataset = CIFAR100(layers_num=sys["ln"], neurons=sys["nn"], dropout=sys["dr"])
    dataset.model.compile(
        optimizer=select_optimizer(sys),
        loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
        metrics=["accuracy"],
    )
    if not sys["aug"]:
        history = dataset.model.fit(
            dataset.train_images,
            dataset.train_labels,
            epochs=sys["e"],
            validation_data=(dataset.test_images, dataset.test_labels),
        )
    else:
        print("Data augmentation")
    plot(
        history,
        f"{sys['ds']}__opt_{sys['o']}__layernum_{sys['ln']}__neurons_{sys['nn']}__droprate_{sys['dr']}__aug{sys['aug']}",
        "ZAD1_badania",
    )
    plot_confusion_matrix(
        dataset,
        f"{sys['ds']}__opt_{sys['o']}__layernum_{sys['ln']}__neurons_{sys['nn']}__droprate_{sys['dr']}__aug{sys['aug']}",
        "ZAD1_badania",
    )

    with open(
        f"SUMMARIES/CIFAR100__neurons_{sys['nn']}__dropout_{sys['dr']}__optimizer_{sys['o']}__aug_{sys['aug']}.txt",
        "w",
    ) as f:
        with redirect_stdout(f):
            dataset.model.summary()
