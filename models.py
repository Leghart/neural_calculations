from keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras import datasets, layers, models
from tensorflow.keras.layers import Conv2D, Dense, Flatten, MaxPooling2D


def normalize_pixel(train, test):
    return train / 255.0, test / 255.0


class MNIST:
    def __init__(self, layers_num, neurons, dropout=0):
        model = models.Sequential()
        model.add(
            layers.Conv2D(
                neurons, kernel_size=(5, 5), strides=(1, 1), activation="tanh", input_shape=(28, 28, 1), padding="same"
            )
        )

        model.add(layers.AveragePooling2D(pool_size=(2, 2), strides=(2, 2), padding="valid"))

        for _ in range(layers_num):
            model.add(layers.Conv2D(neurons, (3, 3), activation="relu"))

        model.add(layers.Conv2D(2 * neurons, kernel_size=(5, 5), strides=(1, 1), activation="tanh", padding="valid"))
        model.add(layers.AveragePooling2D(pool_size=(2, 2), strides=(2, 2), padding="valid"))

        model.add(layers.Flatten())
        model.add(layers.Dense(120, activation="tanh"))
        model.add(layers.Dense(84, activation="tanh"))
        model.add(layers.Dense(10, activation="softmax"))

        if dropout != 0:
            model.add(layers.Dropout(dropout, input_shape=(2,)))

        self.model = model

        (train_images, train_labels), (test_images, test_labels) = datasets.mnist.load_data()
        train_images, test_images = normalize_pixel(train_images, test_images)

        self.train_images = train_images
        self.train_labels = train_labels
        self.test_images = test_images
        self.test_labels = test_labels

        # self.datagen = ImageDataGenerator(width_shift_range=0.1, height_shift_range=0.1, horizontal_flip=True)


class CIFAR10:
    def __init__(self, layers_num, neurons, dropout=0):

        model = models.Sequential()
        model.add(Conv2D(32, kernel_size=(3, 3), activation="relu", input_shape=(32, 32, 3)))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Conv2D(64, kernel_size=(3, 3), activation="relu"))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Flatten())
        model.add(Dense(256, activation="relu"))
        model.add(Dense(128, activation="relu"))
        model.add(Dense(10, activation="softmax"))

        if dropout != 0:
            model.add(layers.Dropout(dropout, input_shape=(2,)))

        print(model.summary())

        self.model = model

        (train_images, train_labels), (test_images, test_labels) = datasets.cifar10.load_data()
        train_images, test_images = normalize_pixel(train_images, test_images)

        self.train_images = train_images
        self.train_labels = train_labels
        self.test_images = test_images
        self.test_labels = test_labels

        self.datagen = ImageDataGenerator(width_shift_range=0.1, height_shift_range=0.1, horizontal_flip=True)
        self.it_train = self.datagen.flow(train_images, train_labels, batch_size=64)


class CIFAR100:
    def __init__(self, layers_num, neurons, dropout=0):
        # Create the model
        model = models.Sequential()
        model.add(Conv2D(neurons, kernel_size=(3, 3), activation="relu", input_shape=(32, 32, 3)))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Conv2D(2 * neurons, kernel_size=(3, 3), activation="relu"))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Flatten())
        model.add(Dense(256, activation="relu"))
        model.add(Dense(128, activation="relu"))
        model.add(Dense(100, activation="softmax"))

        print(model.summary())

        self.model = model
        (train_images, train_labels), (test_images, test_labels) = datasets.cifar10.load_data()
        train_images, test_images = normalize_pixel(train_images, test_images)

        self.train_images = train_images
        self.train_labels = train_labels
        self.test_images = test_images
        self.test_labels = test_labels

        self.datagen = ImageDataGenerator(width_shift_range=0.1, height_shift_range=0.1, horizontal_flip=True)
        self.it_train = self.datagen.flow(train_images, train_labels, batch_size=64)
